# Deploying the Lottery App

We use Kubernetes to deploy the app to our server nodes.

For general information about Kubernetes deployments check out [this WIKI](http://wiki.ourengineroom.com/doku.php?id=projects:praxis:k8s-deployment).

### Things to prepare before you can deploy:
1. You should be logged in to docker registry: `docker login`
2. You should have k8s config file (`~/.kube/config`) configured to be able to connect to our
kubernetes cluster

### Deploying commands:
1. Go to k8s directory: `cd k8s`
2. Update and deploy containers: `./update-containers.sh`
    
    This command builds containers with the latest code, pushes them to our private docker repository
    and then updates `k8s/resources/deployment.yml` file accordingly.
3. Apply Deployment updates to our cluster: `./apply.sh --resource deployment --env staging --context praxis-lottery-app`

    Where `--env` can be either `staging` or `production` and `--context` is the name of the k8s context
    in your `~/.kube/config` that points to our k8s cluster.
    
    You can apply updates for other resources in `k8s/resources/` folder with this command.

### Rolling back deployment:
In case there is some issue with the latest deployment you can rollback to the previous one by
running:
```
kubectl --context praxis-lottery-app --namespace praxis-lottery-app-staging rollout undo deployment.v1.apps/praxis-lottery-app-deployment
```

### Restarting deployment:
In case you updated some config-map or secret that's used in pods you need to restart pods in order
for these updates to be applied to pods.
```sh
kubectl --context praxis-lottery-app --namespace praxis-lottery-app-staging rollout restart deployment/praxis-lottery-app-deployment
```

### Adding/updating env variables on the cluster:
In our Django backend we read env variables from the `.env` file. And on the cluster
we pull this .env file from `env-file` configmap.

In order to update `env-file` configmap:
1. Create `env-file-configmap.yml` in `k8s/resources/env-file-configmap.yml` (use `env-file-configmap-example.yml`)
2. Run `./apply.sh --resource env-file-configmap --env staging --context praxis-lottery-app`
3. Restart the deployment (see the command above)

### Creating dockerregistry secret:
We store our docker containers in private repository, so in order for our k8s cluster
to pull our images we need to provide it credentials:
1. Create an access token in your Docker Hub account
2. Create `dockerregistry-secret.yml` file in `k8s/resources` folder by copying `dockerregistry-secret-template.yml` file.
3. Replace `{{base64(username:password)}}` with a base64-encoded string of `username` and `password` separated by `:`
where `username` is your Docker Hub username and `password` is the access token you created at step `1.`
4. Run `./apply.sh --resource dockerregistry-secret --env staging --context praxis-lottery-app`
5. Restart the deployment (see the command above)

### Configuring HTTPS:
In order to HTTPS work properly on our cluster you need to create `tls-secret` there:
1. Create `reources/domain.key` and `resources/domain.crt` that contain SSL private key and certificate.
2. Run `./apply.sh --resource tls-secret --env staging --context praxis-lottery-app`

### Configuring logging:
We use Papertrail service to aggregate logs to and we do this using Fluentd that's running on each cluster's node.
In order to set the logging up:
1. Run `./upgrade-logging-container.sh` to build our papertrail container and push it to our private repo. This command will
also update the `fluentd-papertrail-daemonset.yml` resource accordingly.
2. Create a logging group in Papertrail where you will send logs to.
3. Create `resources/papertrail-secret.yml` by copying `papertrail-secret-example.yml` file. Specify the proper values for
the logging group and
apply the resource: `./apply.sh --resource papertrail-secret --env staging --context praxis-lottery-app`
4. Apply papertrail deployment resource: `./apply.sh --resource fluentd-papertrail-daemonset --env staging --context praxis-lottery-app`
