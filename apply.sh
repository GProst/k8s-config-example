#!/usr/bin/env bash

RESOURCE=
ENVIRONMENT=

while [[ $# -gt 0 ]] ; do
    key="$1"

    case $key in
        --env)
            ENVIRONMENT="$2"
            shift
            ;;
        --resource)
            RESOURCE="$2"
            shift
            ;;
        --context)
            CONTEXT="$2"
            shift
            ;;
        *)
    esac
    shift
done

# Prepend 4 spaces at each line (otherwise it won't be pasted correctly in configmap file):
NGINX_CONFIG=$(cat ./resources/conf.nginx | awk '{print "    " $0}')

if test -f "./resources/domain.crt" && test -f "./resources/domain.key"; then
  TLS_CRT=$(cat "./resources/domain.crt" | awk '{print "    " $0}')
  TLS_KEY=$(cat "./resources/domain.key" | awk '{print "    " $0}')
fi

export ENVIRONMENT=$ENVIRONMENT
export NGINX_CONFIG=$NGINX_CONFIG
export TLS_CRT=$TLS_CRT
export TLS_KEY=$TLS_KEY
export STAGING_AFFINITY_OPERATOR=$([ "$ENVIRONMENT" == "staging" ] && echo "In" || echo "NotIn")
export REPLICAS=$([ "$ENVIRONMENT" == "staging" ] && echo "2" || echo "2")
envsubst < "./resources/$RESOURCE.yml" | kubectl apply --context $CONTEXT --namespace=praxis-lottery-app-$ENVIRONMENT -f -
