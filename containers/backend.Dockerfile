FROM python:3.8.5-alpine3.12

WORKDIR '/app'

# Install libs required for postgres adapter
RUN \
 apk add --no-cache postgresql-libs && \
 apk add --no-cache --virtual .build-deps gcc musl-dev postgresql-dev
# Install libs required for uvloop package
RUN apk add build-base
# Install pipenv:
RUN pip install pipenv

# Install deps
COPY ./Pipfile ./
COPY ./Pipfile.lock ./
RUN pipenv install --deploy --ignore-pipfile

# Copy the server code:
COPY ./backend ./backend/

EXPOSE 3100

ENTRYPOINT ["/bin/sh", "-c"]

# Copy the start script:
RUN chmod +x ./backend/start-in-container.sh
