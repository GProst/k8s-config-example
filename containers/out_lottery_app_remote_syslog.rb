# This is copied from this one to be able to adjust logging format and program name: https://github.com/georgegoh/fluent-plugin-kubernetes_remote_syslog/blob/master/lib/fluent/plugin/out_kubernetes_remote_syslog.rb
require "fluent/mixin/config_placeholders"

module Fluent
  class LotteryAppRemoteSyslogOutput < Fluent::Output
    Fluent::Plugin.register_output("lottery_app_remote_syslog", self)

    include Fluent::Mixin::ConfigPlaceholders

    config_param :host,     :string
    config_param :port,     :integer, :default => 514
    config_param :facility,     :string,  :default => "user"
    config_param :severity,     :string,  :default => "notice"
    config_param :packet_size,  :integer, :default => 1024

    def initialize
      super
      require "remote_syslog_sender"
    end

    def shutdown
      super
      @logger.close
    end

    def emit(tag, es, chain)
      chain.next
      es.each do |time, record|
        record.each_pair do |k, v|
          if v.is_a?(String)
            v.force_encoding("utf-8")
          end
        end

        @logger ||= RemoteSyslogSender::UdpSender.new(@host, @port,
          facility: @facility,
          severity: @severity,
          packet_size: @packet_size
        )
        @logger.transmit(record['message'], {
          program: record['program'],
          local_hostname: record['kubernetes_host']
        })
      end
    end
  end
end
