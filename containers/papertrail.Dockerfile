FROM fluent/fluentd:v1.9.3-1.0

USER root
RUN apk --no-cache --update add build-base ruby-dev
RUN fluent-gem install remote_syslog_sender
RUN fluent-gem install fluent-mixin-config-placeholders
RUN fluent-gem install fluent-plugin-kubernetes_metadata_filter
RUN apk del build-base ruby-dev
RUN rm -rf /tmp/* /var/tmp/* /var/cache/apk/*

RUN mkdir -p /etc/fluent/plugin
COPY k8s/containers/out_lottery_app_remote_syslog.rb /etc/fluent/plugin/
