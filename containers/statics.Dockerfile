FROM node:12.16.1-alpine3.11

WORKDIR '/app'

ENV NODE_ENV='production'

# Install deps
COPY ./package.json ./
COPY ./package-lock.json ./
RUN npm i --production=false

# Copy code files and assets
COPY ./admin-frontend ./admin-frontend
# Build client app
RUN npx webpack --config ./admin-frontend/webpack.config.js

# Remove source maps
RUN rm ./static/frontend_dist/*.map
RUN rm ./static/frontend_dist/*.map.gz

# Remove source files
RUN rm -rf ./admin-frontend

# Copy static files:
COPY ./static ./static

