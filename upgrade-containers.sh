#!/usr/bin/env bash

NEW_VERSION=$(date +'%d.%m.%y_%H-%M')

# Build new docker containers:
build_container () {
  DOCKER_BUILDKIT=1 docker build ../ \
    -f "./containers/$1.Dockerfile" \
    -t "lotteryapplication:$1-$NEW_VERSION"
}
build_container "backend" &&
build_container "statics" &&

# Add tags so that we could push the new containers to docker registry:
docker tag "lotteryapplication:backend-$NEW_VERSION" "praxisfintech/lotteryapplication:backend-$NEW_VERSION" &&
docker tag "lotteryapplication:statics-$NEW_VERSION" "praxisfintech/lotteryapplication:statics-$NEW_VERSION" &&

docker push "praxisfintech/lotteryapplication:backend-$NEW_VERSION" &&
docker push "praxisfintech/lotteryapplication:statics-$NEW_VERSION" &&

# Update deployment file:
sed -i '' -E s"/(lotteryapplication:[^0-9]*).+$/\1$NEW_VERSION/" ./resources/deployment.yml &&
sed -i '' -E s"/(app_release__).+$/\1$NEW_VERSION/" ./resources/deployment.yml
