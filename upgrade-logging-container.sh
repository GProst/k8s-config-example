#!/usr/bin/env bash

NEW_VERSION=$(date +'%d.%m.%y_%H-%M')

# Build new docker container:
DOCKER_BUILDKIT=1 docker build ../ \
  -f "./containers/papertrail.Dockerfile" \
  -t "lotteryapplication:papertrail-$NEW_VERSION"

# Add tags so that we could push the new containers to docker registry:
docker tag "lotteryapplication:papertrail-$NEW_VERSION" "praxisfintech/lotteryapplication:papertrail-$NEW_VERSION" &&

docker push "praxisfintech/lotteryapplication:papertrail-$NEW_VERSION" &&

# Update daemonset file:
sed -i '' -E s"/(lotteryapplication:[^0-9]*).+$/\1$NEW_VERSION/" ./resources/fluentd-papertrail-daemonset.yml
